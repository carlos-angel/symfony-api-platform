<?php

declare(strict_types=1);

namespace App\Exception\User;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserNotFoundException extends NotFoundHttpException
{
    private const MESSAGE = 'User with email %s not found';
    private const MESSAGE_FROM_USER_ID_AND_TOKEN = 'User with id %s and token %s not found';
    private const USER_ID_AND_RESET_PASSWORD_TOKEN = 'User with id %s and resetPasswordToken %s not found';
    private const USER_ID = 'User with id %s not found';

    public static function fromEmail(string $email): self
    {
        throw new self(\sprintf(self::MESSAGE, $email));
    }

    public static function fromUserIdAndToken(string $id, string $token): self
    {
        throw new self(\sprintf(self::MESSAGE_FROM_USER_ID_AND_TOKEN, $id, $token));
    }

    public static function fromUserIdAndResetPasswordToken(string $id, string $resetPasswordToken): self
    {
        throw new self(\sprintf(self::USER_ID_AND_RESET_PASSWORD_TOKEN, $id, $resetPasswordToken));
    }

    public static function fromUserId(string $id): self
    {
        throw new self(\sprintf(self::USER_ID, $id));
    }
}
