<?php

declare(strict_types=1);

namespace App\Tests\Functional\Category;

use Symfony\Component\HttpFoundation\Response;

class UpdateCategoryTest extends CategoryTestBase
{
    public function testUpdateCategory(): void
    {
        $payload = ['name' => 'new category name'];
        $peterCategoryId = $this->getPeterExpenseCategoryId();

        self::$peter->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterCategoryId), [], [], [], \json_encode($payload));

        $response = self::$peter->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($payload['name'], $responseData['name']);
    }

    public function testUpdateGroupCategory(): void
    {
        $payload = ['name' => 'new category name'];
        $peterGroupCategoryId = $this->getPeterGroupExpenseCategoryId();

        self::$peter->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterGroupCategoryId), [], [], [], \json_encode($payload));

        $response = self::$peter->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($payload['name'], $responseData['name']);
    }

    public function testUpdateAnotherCategory(): void
    {
        $payload = ['name' => 'new category name'];
        $peterCategoryId = $this->getPeterExpenseCategoryId();

        self::$brian->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterCategoryId), [], [], [], \json_encode($payload));

        $response = self::$brian->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUpdateAnotherGroupCategory(): void
    {
        $payload = ['name' => 'new category name'];
        $peterGroupCategoryId = $this->getPeterGroupExpenseCategoryId();

        self::$brian->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterGroupCategoryId), [], [], [], \json_encode($payload));

        $response = self::$brian->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }
}
