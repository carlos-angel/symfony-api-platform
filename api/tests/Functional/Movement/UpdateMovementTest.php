<?php

declare(strict_types=1);

namespace App\Tests\Functional\Movement;

use Symfony\Component\HttpFoundation\Response;

class UpdateMovementTest extends MovementTestBase
{
    public function testUpdateMovement(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getPeterExpenseCategoryId()),
            'amount' => 500.05,
        ];

        $peterMovementId = $this->getPeterMovementId();
        self::$peter->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterMovementId), [], [], [], \json_encode($payload));

        $response = self::$peter->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($payload['category'], $responseData['category']);
        $this->assertEquals($payload['amount'], $responseData['amount']);
    }

    public function testUpdateGroupMovement(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getPeterGroupExpenseCategoryId()),
            'amount' => 500.05,
        ];

        $peterGroupMovementId = $this->getPeterGroupMovementId();
        self::$peter->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterGroupMovementId), [], [], [], \json_encode($payload));

        $response = self::$peter->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($payload['category'], $responseData['category']);
        $this->assertEquals($payload['amount'], $responseData['amount']);
    }

    public function testUpdateAnotherUserMovement(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getPeterExpenseCategoryId()),
            'amount' => 500.05,
        ];

        $peterMovementId = $this->getPeterMovementId();
        self::$brian->request('PUT', \sprintf('%s/%s', $this->endpoint, $peterMovementId), [], [], [], \json_encode($payload));

        $response = self::$brian->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUpdateAnotherGroupMovement(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getPeterExpenseCategoryId()),
            'amount' => 500.05,
        ];

        $brianGroupMovementId = $this->getBrianGroupMovementId();
        self::$peter->request('PUT', \sprintf('%s/%s', $this->endpoint, $brianGroupMovementId), [], [], [], \json_encode($payload));

        $response = self::$peter->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUpdateMovementWithAnotherUserCategory(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getBrianExpenseCategoryId()),
            'amount' => 500.05,
        ];

        self::$peter->request(
            'PUT',
            \sprintf('%s/%s', $this->endpoint, $this->getPeterMovementId()),
            [],
            [],
            [],
            \json_encode($payload)
        );

        $response = self::$peter->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testUpdateMovementWithAnotherGroupCategory(): void
    {
        $payload = [
            'category' => \sprintf('/api/v1/categories/%s', $this->getBrianGroupExpenseCategoryId()),
            'amount' => 500.05,
        ];

        self::$peter->request(
            'PUT',
            \sprintf('%s/%s', $this->endpoint, $this->getPeterMovementId()),
            [],
            [],
            [],
            \json_encode($payload)
        );

        $response = self::$peter->getResponse();

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }
}
