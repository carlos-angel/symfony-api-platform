<?php

declare(strict_types=1);

namespace Mailer\Messenger\Message;


class GroupRequestMessage
{
    private string $groupId;
    private string $userId;
    private string $token;
    private string $requesterName;
    private string $groupName;
    private string $receiverEmail;

    public function __construct(string $groupId, string $userId, string $token, string $requesterName, string $groupName, string $receiverEmail)
    {
        $this->groupId = $groupId;
        $this->userId = $userId;
        $this->token = $token;
        $this->requesterName = $requesterName;
        $this->groupName = $groupName;
        $this->receiverEmail = $receiverEmail;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId(string $groupId): void
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getRequesterName(): string
    {
        return $this->requesterName;
    }

    /**
     * @param string $requesterName
     */
    public function setRequesterName(string $requesterName): void
    {
        $this->requesterName = $requesterName;
    }

    /**
     * @return string
     */
    public function getGroupName(): string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName(string $groupName): void
    {
        $this->groupName = $groupName;
    }

    /**
     * @return string
     */
    public function getReceiverEmail(): string
    {
        return $this->receiverEmail;
    }

    /**
     * @param string $receiverEmail
     */
    public function setReceiverEmail(string $receiverEmail): void
    {
        $this->receiverEmail = $receiverEmail;
    }

}